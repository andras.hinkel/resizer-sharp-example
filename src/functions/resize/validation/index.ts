import { ResizeEvent } from "@functions/resize/schema";

const isObjectWithProperty = (
  raw: unknown,
  property: string
): raw is { [property: string]: unknown } =>
  typeof raw === "object" && raw !== null && property in raw;

const isResizeEvent = (event: unknown): event is ResizeEvent => {
  if (isObjectWithProperty(event, "Records") && Array.isArray(event.Records)) {
    return event.Records.every(
      (r) =>
        isObjectWithProperty(r, "s3") &&
        isObjectWithProperty(r.s3, "bucket") &&
        isObjectWithProperty(r.s3.bucket, "name") &&
        typeof r.s3.bucket.name === "string" &&
        isObjectWithProperty(r.s3, "object") &&
        isObjectWithProperty(r.s3.object, "key") &&
        typeof r.s3.object.key === "string"
    );
  }

  return false;
};

export default isResizeEvent;
