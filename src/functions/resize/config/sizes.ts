export type SizeName =
  | "xl"
  | "l"
  | "m2"
  | "m"
  | "s2"
  | "t"
  | "b312"
  | "b469"
  | "b625"
  | "b940"
  | "sq"
  | "sq2";

type ImageSize = {
  name: SizeName;
  width: number;
  height: number;
};

const sizes: Array<ImageSize> = [
  {
    name: "xl",
    width: 1600,
    height: 1600,
  },
  {
    name: "l",
    width: 1024,
    height: 1024,
  },
  {
    name: "m2",
    width: 450,
    height: 450,
  },
  {
    name: "m",
    width: 500,
    height: 500,
  },
  {
    name: "s2",
    width: 220,
    height: 220,
  },
  {
    name: "t",
    width: 100,
    height: 100,
  },
];

export default sizes;
