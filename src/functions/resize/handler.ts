import "source-map-support/register";
const AWS = require("aws-sdk");
import { ResizeEvent } from "./schema";
import isResizeEvent from "@functions/resize/validation";
import sizes, { SizeName } from "./config/sizes";
import * as sharp from "sharp";
import * as path from "path";

const s3 = new AWS.S3();
const { SOURCE_BUCKET, DESTINATION_BUCKET } = process.env;

const resize = async (event: ResizeEvent) => {
  for (const r of event.Records) {
    const sourceKey = r.s3.object.key;
    const { name: sourceFilename, ext: sourceFileExtension } =
      path.parse(sourceKey);

    const params = { Bucket: SOURCE_BUCKET, Key: sourceKey };

    const sourceImageBuffer = (await s3.getObject(params).promise())
      .Body as Buffer;

    const resizedImages = await Promise.allSettled(
      sizes.map(({ width, height, name }) => ({
        size: name,
        image: sharp(sourceImageBuffer).resize({
          width,
          height,
          fit: "contain",
        }),
      }))
    );

    const rejectedResizes = resizedImages.filter(
      ({ status }) => status === "rejected"
    ) as Array<PromiseRejectedResult>;

    rejectedResizes.forEach((i) =>
      console.error(`Failed to resize image: ${i}`)
    );

    const successfulResizes = resizedImages.filter(
      ({ status }) => status === "fulfilled"
    ) as PromiseFulfilledResult<{ size: SizeName; image: sharp.Sharp }>[];

    const writeResults = await Promise.allSettled(
      successfulResizes.map(async (i) =>
        s3
          .putObject({
            Bucket: DESTINATION_BUCKET,
            Key: `${sourceFilename}-${i.value.size}${sourceFileExtension}`,
            Body: await i.value.image.toBuffer(),
          })
          .promise()
      )
    );

    const rejectedWriteResults = writeResults.filter(
      ({ status }) => status === "rejected"
    ) as Array<PromiseRejectedResult>;

    rejectedWriteResults.forEach((i) =>
      console.error(`Failed to write resized image to s3: ${i.reason}`)
    );
  }

  return {
    status: 200,
    body: JSON.stringify({
      event,
    }),
  };
};

export const main = async (event: unknown) => {
  if (!isResizeEvent(event)) {
    throw new Error(`Malformed event: ${event}`);
  }

  await resize(event);
};
