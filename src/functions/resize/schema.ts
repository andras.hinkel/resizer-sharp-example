export type ResizeEvent = {
  Records: Array<{
    s3: {
      bucket: {
        name: string;
      },
      object: {
        key: string;

      }
    }
  }>
};